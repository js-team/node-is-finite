# Installation
> `npm install --save @types/is-finite`

# Summary
This package contains type definitions for is-finite (https://github.com/sindresorhus/is-finite#readme).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/is-finite

Additional Details
 * Last updated: Thu, 29 Dec 2016 23:09:06 GMT
 * Library Dependencies: none
 * Module Dependencies: none
 * Global values: none

# Credits
These definitions were written by Mohamed Hegazy <https://github.com/mhegazy>.
